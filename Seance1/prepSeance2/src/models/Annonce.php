<?php

namespace Bdd\models;

use Illuminate\Database\Eloquent\Model;
use Bdd\models\Photo;
use Bdd\models\Categorie;

class Annonce extends Model {

	protected $table = "annonce";
	protected $primaryKey = "id";
	public $timestamps = false;

	public function photos() {
		return $this->hasMany(Photo::class);
	}

	public function categories() {
		return $this->belongsToMany(Categorie::class, 'categorie_photo', 'annonce_id', 'categorie_id');
	}

}
