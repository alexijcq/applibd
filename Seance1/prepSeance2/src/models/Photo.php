<?php

namespace Bdd\models;

use Illuminate\Database\Eloquent\Model;
use Bdd\models\Annonce;

class Photo extends Model {

	protected $table = "photo";
	protected $primaryKey = "id";
	public $timestamps = false;
	protected $guarded = [];

	public function annonce() {
		return $this->belongsTo(Annonce::class);
	}

}
