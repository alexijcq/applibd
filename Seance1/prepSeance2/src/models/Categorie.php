<?php

namespace Bdd\models;

use Illuminate\Database\Eloquent\Model;
use Bdd\models\Annonce;

class Categorie extends Model {

	protected $table = "categorie";
	protected $primaryKey = "id";
	public $timestamps = false;

	public function annonces() {
		return $this->belongsToMany(Annonce::class, 'categorie_photo', 'categorie_id', 'annonce_id');
	}

}
