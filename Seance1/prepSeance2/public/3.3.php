<?php

require '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('../src/config/db.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
 * Les annonces possédant plus de 3 photos
 */

use Bdd\models\Annonce;
use Bdd\models\Photo;

$annonces = Annonce::has('photos', '>', 3)->get();

foreach($annonces as $annonce)
	echo "$annonce->id - $annonce->titre\n";
