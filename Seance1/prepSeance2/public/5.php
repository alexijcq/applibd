<?php

require '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('../src/config/db.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
 * Ajouter l'annonce 22 aux catégories 42 et 73
 */

use Bdd\models\Annonce;
/*
$categorie42 = new Annonce();
$categorie73 = new Annonce();

$categorie42->id = 22;
$categorie73->id = 22;

$categorie42->id = 42;
$categorie73->id = 73;

$categorie42->save();
$categorie73->save();

echo "La photo est sauvegardée.";*/

$annonce = Annonce::find(22)->categorie()->saveMany([
    new \Bdd\models\Categorie(['categorie_id' => 42]),
    new \Bdd\models\Categorie(['categorie_id' => 73])
]);
