<?php

require '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('../src/config/db.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
 * Les photos de l'annonce 22 dont la taille en octets est > 100 000
 */

use Bdd\models\Annonce;

$photos = Annonce::findOrFail(22)->photos()->where('taille_octet', '>', 100000)->get();

foreach($photos as $photo)
	echo "$photo->id - $photo->file\n";
