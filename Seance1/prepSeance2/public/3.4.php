<?php

require '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('../src/config/db.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
 * Les annonces possédant des photos dont la taille est > 100 000
 */

use Bdd\models\Annonce;
use Bdd\models\Photo;


//$annonces = Photo::where('taille_octet', '>', '100000')->annonce()->distinct()->get();

//$annonces = Annonce::has('photos')->photos()->where('taille_octet', '>', 100000)->get();


$annonces = Annonce::whereHas('photos', function($query) {
   $query->where('taille_octet', '>', 100000);
})->get();



//print($annonce->id);
//$annonces = $annonce->annonce()->get();


//$annonces = Photo::where('taille_octet', '>', '100000')->annonce()->distinct()->get();

foreach($annonces as $annonce)
	echo "$annonce->id - $annonce->titre\n";
