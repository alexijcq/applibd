<?php

require '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('../src/config/db.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
 * Ajouter une photo à l'annonce 22
 */

use Bdd\models\Photo;

$photo = new Photo();
$photo->file = 'Super photo question 4';
$photo->date = '2019-01-13';
$photo->taille_octet = 150000;
$photo->id_annonce = 22;
$photo->save();

echo "La photo est sauvegardé.";
