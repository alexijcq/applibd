<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 25/03/2019
 * Time: 15:34
 */

require 'vendor/fzaninotto/faker/src/autoload.php';
require 'vendor/autoload.php';


use Illuminate\Database\Capsule\Manager as Manager;
use applibd\models\User as User;
use applibd\models\Commentaire as Commentaire;

error_reporting(E_ALL);
ini_set('display_errors', 1);

$db = new Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

Manager::connection()->enableQueryLog();


$app = new \Slim\Slim;
$faker = Faker\Factory::create();



for($i = 0 ; $i<1000 ; $i++) {
    $user = new User();
    $nom = $faker->lastName;
    $email = $faker->email;
    $user->nom = $nom;
    $user->prenom = $faker->firstName;
    $user->email = $email;
    $user->adresse = $faker->address;
    $user->numTel = $faker->phoneNumber;
    $user->dateNaiss = $faker->date($format = 'Y-m-d', $max = 'now');
    $user->save();
}

$tabUser = User::select('id')->get();

for($j = 0; $j<2000; $j++){
    $com = new Commentaire();
    $res = $tabUser[rand(0,sizeof($tabUser)-1)]["id"];
    $com->idUser = $res;
    $com->idJeu = random_int(1,47948);
    $com->titre = $faker->realText(40,1);
    $com->contenu = $faker->realText(200,2);
    $com->dateCrea = $faker->date($format = 'Y-m-d', $max = 'now');
    $com->created_at = $faker->dateTime($max = 'now', $timezone = null);
    $com->updated_at = $faker->dateTime($max = 'now', $timezone = null);
    $com->save();
}



