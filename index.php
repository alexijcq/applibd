
<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 03/03/2019
 * Time: 14:37
 */

require 'vendor/autoload.php' ;

use Illuminate\Database\Capsule\Manager as Manager;
use applibd\models\Game as Game;
use applibd\models\Platform as Platform;
use applibd\models\Company as Company;
use applibd\vues\VuePrincipal;

error_reporting(E_ALL);
ini_set('display_errors', 1);

$db = new Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

Manager::connection()->enableQueryLog();


$app = new \Slim\Slim;

$app->get('/', function () {
    $vuePrincipal = new VuePrincipal("elem", "ALL_VIEW");
    $vuePrincipal->render();
});

$app->get('/projet1/question/:id', function ($id) {
    switch ($id) {
        case '1':
            $c = new \applibd\controleurs\ControleurGame();
            $c->afficherJeuxMario();
            break;
        case '2':
            $c = new \applibd\controleurs\ControleurCompany();
            $c->compagniesJap();
            break;
        case '3':
            $c = new \applibd\controleurs\ControleurPlatform();
            $c->grossesPlatforms();
            break;
        case '4':
            $c = new \applibd\controleurs\ControleurGame();
            $c->afficherJeuxAPartir();
            break;
        case '5':
            $c = new \applibd\controleurs\ControleurGame();
            $c->paginationJeux(1);
            break;
        default:
            // code...
            break;
    }
    {

    }
});

$app->post('/projet1/question/5', function ($request = null) {
      $c = new \applibd\controleurs\ControleurGame();
	if(isset($request))
         $c->paginationJeux($request->getParam('nbPage'));
        else
         $c->paginationJeux(1);
    {

    }
});




$app->get('/projet2/question/:id', function ($id) {
    switch ($id) {
        case '1':
            // $c = new \applibd\controleurs\ControleurGame();
            // $c->afficherPersonnageJeu12342();
            $c = new \applibd\controleurs\TD2_controleur();
            $c->afficherPersonnageJeu12342();
            break;
        case '2':
            $c = new \applibd\controleurs\TD2_controleur();
            $c->afficherJeuxDebutentMario();
            break;

        /*case '2':
            $c = new applibd\controleurs\ControleurGame();
            $c->afficherPersonnagesJeuxDebutMario();
            break;¨*/

        case '3':
            $c = new \applibd\controleurs\TD2_controleur();
            $c->jeuxDevParSony();
            break;

        case '4':
            $c = new \applibd\controleurs\TD2_controleur();
            $c->ratingBoardMario();
            break;

        case '5':
            $c = new \applibd\controleurs\TD2_controleur();
            $c->mario3pers();
            break;

        case '6':
            $c = new \applibd\controleurs\TD2_controleur();
            $c->marioRating3();
            break;

        case '7':
            $c = new \applibd\controleurs\TD2_controleur();
            $c->jeuxCompagniesInc3Plus();
            break;

        default:
            // code...
            break;
    }
    {

    }



});


$app->get("/projet3/question/:id", function ($id){
    switch ($id){
        case 1 :
            $c = new \applibd\controleurs\TD3_controleur();
            $c->tempsTousLesJeux();
            break;
        case 2 :
            $c = new \applibd\controleurs\TD3_controleur();
            $c->tempsPourJeuxContenantMario();
            break;
        case 3 :
            $c = new \applibd\controleurs\TD3_controleur();
            $c->tempsPourJeuxDebutantMario();
            break;
        case 4 :
            $c = new \applibd\controleurs\TD3_controleur();
            $c->tempsPourJeuxDebutantMarioRating3();
            break;
        case 5 :
            $c = new \applibd\controleurs\TD3_controleur();
            $c->partie2_debute();
            break;
        case 6 :
            $c = new \applibd\controleurs\TD3_controleur();
            $c->partie2_contient();
            break;
        case 7:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->pays();
            break;
        case 8:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->listerMario();
            break;
        case 9:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->perso12342();
            break;
        case 10:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->listerPersoJeuxMarioPremiere();
            break;
        case 11:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->afficherPersoJeuxMario();
            break;
        case 12:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->jeuxDevParSony();
            break;
        case 20:
            $c = new \applibd\controleurs\TD3_controleur();
            $c->afficherLog();
            break;
    }
});

$app->get("/projet4/question/:id/:idC", function ($id, $idC) {
    switch ($id) {
        case 1 :
            $c = new \applibd\controleurs\TD4_controleur();
            $c->q1($idC);
            break;
        case 2 :
            $c = new \applibd\controleurs\TD4_controleur();
            $c->q2();
            break;
    }
});

//pour l'api
$app->get("/api/games", function (){
   $c = new \applibd\controleurs\TD5_controleur();
   if(isset($_GET["page"])){
       $c->partie3();
   }else{
       $c->partie2();
   }

})->name("jeux");

$app->get("/api/games/:id/comments", function ($id){
    $c = new \applibd\controleurs\TD5_controleur();
    $c->partie5($id);
})->name("comments");

$app->get("/api/games/:id/characters", function ($id){
    $c = new \applibd\controleurs\TD5_controleur();
    $c->characters($id);
})->name("characters");

$app->get("/api/games/:id", function ($id){
    $c = new \applibd\controleurs\TD5_controleur();
    $c->partie1($id);
})->name("jeu");

$app->get("/api/platform/:id", function ($id){
   $c = new \applibd\controleurs\TD5_controleur();
   $c->platform($id);
})->name("platform");

$app->get("/api/characters/:id", function ($id){
   $c = new \applibd\controleurs\TD5_controleur();
   $c->character($id);
})->name("1character");


$app->run();
