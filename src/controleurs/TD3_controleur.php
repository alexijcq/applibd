<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 25/03/2019
 * Time: 13:06
 */

namespace applibd\controleurs;


use applibd\models\Character;
use applibd\models\Company;
use applibd\models\Game;
use Illuminate\Database\Capsule\Manager as DB;

class TD3_controleur{

    public function tempsTousLesJeux(){
        $tempsDeb = microtime(true);

        //mauvaise technique mais bon problème de mémoire
        ini_set('memory_limit', '-1');

        $jeux = Game::all();

        $tempsFin = microtime(true);

        $tempsTotal = $tempsFin-$tempsDeb;

        echo "Il aura fallu $tempsTotal secondes pour lister tous les jeux";
    }

    public function tempsPourJeuxContenantMario(){
        $tempsDb = microtime(true);

        $games = Game::where('name', 'LIKE', '%mario%')->get();

        $tempsFin = microtime(true);

        $tempsTot = $tempsFin-$tempsDb;

        echo "Il aura fallu $tempsTot secondes pour lister les jeux contenant mario";
    }

    public function tempsPourJeuxDebutantMario(){
        $tempsDb = microtime(true);

        $games = Game::where('name', 'LIKE', 'Mario%')->get();

        $tempsFin = microtime(true);

        $tempsTot = $tempsFin-$tempsDb;

        echo "Il aura fallu $tempsTot secondes pour lister les jeux débutant par mario";
    }

    public function tempsPourJeuxDebutantMarioRating3(){
        $tempsDb = microtime(true);

        $games = Game::where('name', 'LIKE', 'Mario%')->get();

        foreach ($games as $game){
            $rating = $game->ratings()->where("name", "LIKE", '%3+%', 'and','game_id','=',$game->id)->get();
        }

        $tempsFin = microtime(true);

        $tempsTot = $tempsFin-$tempsDb;

        echo "Il aura fallu $tempsTot secondes pour lister les jeux débutant par mario ayant un rating de 3";
    }

    public function partie2_debute(){
        $td1 = microtime(true);
        Game::where('name', 'LIKE', 'Mario%')->get();
        $tf1 = microtime(true);

        $td2 = microtime(true);
        Game::where('name', 'LIKE', 'Sonic%')->get();
        $tf2 = microtime(true);

        $td3 = microtime(true);
        Game::where('name', 'LIKE', 'The%')->get();
        $tf3 = microtime(true);

        $t1 = $tf1 - $td1;
        $t2 = $tf2 - $td2;
        $t3 = $tf3 - $td3;

        echo "voici le temps 1 : $t1, le 2 : $t2, le 3 : $t3";
        //résultat avant index :
        //voici le temps 1 : 0.29594302177429, le 2 : 0.27758121490479, le 3 : 0.31421685218811
        //après les indexs
        //voici le temps 1 : 0.070664882659912, le 2 : 0.0087840557098389, le 3 : 0.081568956375122



    }


    public function partie2_contient(){
        $td1 = microtime(true);
        Game::where('name', 'LIKE', '%Mario%')->get();
        $tf1 = microtime(true);

        $td2 = microtime(true);
        Game::where('name', 'LIKE', '%Sonic%')->get();
        $tf2 = microtime(true);

        $td3 = microtime(true);
        Game::where('name', 'LIKE', '%The%')->get();
        $tf3 = microtime(true);

        $t1 = $tf1 - $td1;
        $t2 = $tf2 - $td2;
        $t3 = $tf3 - $td3;

        echo "voici le temps 1 : $t1, le 2 : $t2, le 3 : $t3";
        //résultat :
        //voici le temps 1 : 0.27501106262207, le 2 : 0.26826882362366, le 3 : 0.38824200630188
        //cela est plus long et provient du fait qu'on recherche une chaine au milieu d'une autre
        //il n'est donc pas possible de trier alphabétiquement



    }

    public function pays(){
        $td1 = microtime(true);
        $companies = Company::where('location_country','=', 'Japan')->get();
        $tf1 = microtime(true);

        $t1 = $tf1 - $td1;

        echo "voici le temps pour lister les compagnies du japon : $t1";
        //résultat :
        //voici le temps pour lister les compagnies du japon : 0.20141816139221
        // après index sur nom:
        //voici le temps pour lister les compagnies du japon : 0.069748878479004

    }

    public function listerMario(){
        $games = Game::where('name', 'LIKE', '%mario%')->get();
        $this->afficherLog();
    }

    public function perso12342(){
        $perso = Game::where('id','LIKE','12342')->first()->personnages;
        $this->afficherLog();
    }

    public function listerPersoJeuxMarioPremiere(){
        $persosMario = Game::with("personnagesOntFaitPremiereApparition")
            ->where('name', 'LIKE', 'Mario%')->get();

        $this->afficherLog();
    }

    public function afficherPersoJeuxMario(){
        $persosMario = Game::with("personnages")->where("name", "like", "%mario%")->get();
        $this->afficherLog();
    }

    public function jeuxDevParSony(){
        //mauvaise technique mais bon problème de mémoire
        ini_set('memory_limit', '-1');


       /* $jeuxDevSony = Game::with(array("developers"=>function($val){
            $val->where("name", "LIKE", "%Sony%");
        }))->get();*/

        $jeuxDev = Company::with("games")->where("name", "like", "%Sony%")->get();

        $this->afficherLog();
    }

    public function afficherLog(){
        $logs = DB::getQueryLog();

        $dernierLog = $logs[sizeof($logs)-1];

        $query = $dernierLog["query"];
        $param = $dernierLog["bindings"][0];
        $time = $dernierLog["time"]/1000;


        echo "<pre>";
        var_dump($logs);

        $chaine = <<< END
        Requête $query
        <br>
        Paramètres = $param
        <br>
        Temps $time
END;

       // echo "<br>" . $chaine;

    }
}