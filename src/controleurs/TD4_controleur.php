<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 26/03/2019
 * Time: 17:12
 */

namespace applibd\controleurs;

use applibd\models\User;
use applibd\models\Commentaire;
use Illuminate\Database\Capsule\Manager as DB;

class TD4_controleur{

    public function q1($id){
        $commentaire = Commentaire::where('idUser',"=", $id)->orderBy('dateCrea', 'DESC')->get();
        echo "Voici les commentaires de l'utilisateur : ".$id."<br>";
        foreach ($commentaire as $j){
            echo
                $j->dateCrea." : ".$j->contenu."<br>";
        }

    }



    public function q2(){
            $jeu = User::has('commentaires','>',5)->get();
            $nb = 0;
            echo "Voici les utilisateurs ayant posté plus de 5 commentaire  : "."<br>";
            foreach ($jeu as $j){
                echo $j->nom." ".$j->prenom." \n<br>";
                $nb++;
            }
            echo "Nombre d'utilisateur ayant plus de 5 commentaires : ".$nb;
    }
}