<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 23/03/2019
 * Time: 17:30
 */

namespace applibd\controleurs;


use applibd\models\Company;
use applibd\models\Game;
use applibd\models\Rating_Board;

class TD2_controleur{

    //Q1
    public function afficherPersonnageJeu12342(){
        $jeu = Game::where('id','LIKE','12342')->first();

        $perso = $jeu->personnages;

        $vue = new \applibd\vues\VuePrincipal($perso,"P2Q1");
        $vue -> render();
    }

    //Q2
    public function afficherJeuxDebutentMario(){
        $jeux = Game::where('name','LIKE','Mario%')->get();


        $vue = new \applibd\vues\VuePrincipal($jeux,"P2Q2");
        $vue -> render();
    }

    //3
    public function jeuxDevParSony(){
        $company = Company::where('name','like','%sony%')->get();

        $vue = new \applibd\vues\VuePrincipal($company,"P2Q3");
        $vue -> render();
    }

    //4 ratings board mario
    public function ratingBoardMario(){
        $games = Game::where('name', 'LIKE', '%mario%')->get();

        $tabElems = [];

        foreach($games as $marioGame) {
            $ratingsMario = $marioGame->ratings;
            foreach($ratingsMario as $ratingMario) {
                $ratingBoard = Rating_Board::where('id', '=',$ratingMario->rating_board_id)->first();
                $tabElems[] = $ratingBoard;

            }
        }


        $vue = new \applibd\vues\VuePrincipal($tabElems,"P2Q4");
        $vue -> render();
    }

    //5
    public function mario3pers(){
        $tabNoms = [];


        $jeux = Game::where('name','LIKE','Mario%')->get();
        foreach($jeux as $game) {
            if($game->personnages()->count() > 3) {
                $tabNoms[] = $game->name;
            }
        }

        $vue = new \applibd\vues\VuePrincipal($tabNoms,"P2Q5");
        $vue -> render();
    }

    //6
    public function marioRating3(){
        $tabJeux = [];

        $marioGames = Game::where('name','LIKE','Mario%')->get();
        foreach($marioGames as $game) {
            $ratings = $game->ratings()->where('name','like','%3+%','and','game_id','=',$game->id)->count();
            if($ratings >= 1) {
                $tabJeux[] = $game->name;
            }

        }

        $vue = new \applibd\vues\VuePrincipal($tabJeux,"P2Q6");
        $vue -> render();

    }

    //7
    public function jeuxCompagniesInc3Plus(){

        $tabJeux = [];


        $incCompanies= Company::where('name','like','%Inc.%')->get();
        foreach($incCompanies as $incCompany){
            $incMarioGames = $incCompany->jeuxPublies()->where('name','like','%mario%')->get();
            foreach($incMarioGames as $incMarioGame) {

                $respectRating = $incMarioGame->ratings()->where('name','like','%3+%')->count();
                if($respectRating >= 1) {
                    $tabJeux[] = $incMarioGame->name;
                }
            }
        }

        $vue = new \applibd\vues\VuePrincipal($tabJeux,"P2Q7");
        $vue -> render();
    }

}