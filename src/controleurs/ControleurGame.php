<?php

namespace applibd\controleurs;

use applibd\models\Character;
use applibd\models\Game;
use applibd\vues\VuePrincipal;
class ControleurGame
{

    public function afficherJeuxMario()
    {

        $games = Game::where('name', 'LIKE', '%mario%')->get();
        $vue = new \applibd\vues\VuePrincipal($games, "P1Q1");
        $vue->render();
    }

    public function afficherJeuxAPartir()
    {
        echo "<h3>Question n°4 : liste de 442 jeux à partir du 21173ème</h3>";

        $jeux = Game::where('id', '=', '21173')->first();
        for ($i = 0; $i < 443; $i++) {
            $jeu = Game::where('id', '=', '21173' + $i)->first();
            echo "id: $jeu->id,  nom: $jeu->name <br>";
        }
    }

  public function paginationJeux($id){
    echo "<h3>Question n°5 : lister les jeux, afficher leur nom et deck, en paginant (taille des pages : 500)</h3>";
    $idmax = Game::select('id')->get();
    $nbPages = (count($idmax)/500)+1;
    $numPage = $id;

	$formulaireQuestion5 = "<form action='' method='post'><select name='numeroPage'>";
	$formulaireQuestion5 .= " <option value='1'></option> ";
    for($i = 1 ; $i < $nbPages ; $i++) {
        $formulaireQuestion5 .= " <option value='$i'>$i</option> ";
    }
    $formulaireQuestion5 .= " </select> <input type='submit' value='Valider'> </form> ";
    echo $formulaireQuestion5;

        // Lors du passage avec Slim, utilisez la methode post de Slim.
        if (isset($_POST['numeroPage'])) {
            $numPage = $_POST['numeroPage'];
        }

        $valeurMax = $numPage * 500;
        for ($valeurMin = $valeurMax - 499; $valeurMin < $valeurMax + 1; $valeurMin++) {
            $jeuAffiche = Game::where('id', '=', $valeurMin)->first();
            if ($jeuAffiche != null) {
                echo "<ul>";
                echo "<li>Identifiant du jeu: $jeuAffiche->id</li>";
                echo "<li>Nom du jeu: $jeuAffiche->name</li>";
                echo "<li>Description du deck: $jeuAffiche->deck</li>";
                echo "</ul>";
            }

        }
	//echo "</body></html>";
 	}

   /* public function afficherPersonnageJeu12342()
    {
        echo "<h3>Afficher (name , deck) les personnages du jeu 12342</h3>";


        $perso = Game::find(12342)->personnages()->get();
        foreach ($perso as $value) {
            echo "nom: $value->name,  deck: $value->deck <br>";
        }
    }*/

    public function afficherPersonnageJeu12342(){
        $perso = Game::where('id','LIKE','12342')->get();
        $vue = new \applibd\vues\VuePrincipal($perso,"P2Q1");
        $vue -> render();
    }

    public function afficherPersonnagesJeuxDebutMario()
    {
        $res = "<h3>Question 2 : les personnages des jeux commençant par mario</h3>";
        $marioGames = Game::where('name', 'LIKE', 'Mario%')->get();
        foreach ($marioGames as $marioGame) {
            foreach ($marioGame->personnages as $personnage) {
                $res .= "Personnage du jeu commencant par mario : $personnage->name<br>";
            }
        }
        echo $res;
    }

    public function ratingBoardMario() {
        $games = Game::where('name', 'LIKE', '%mario%')->get();
        $res = "<h3>Question n°4 : rating boards des jeux contenant Mario</h3>";
        foreach($games as $marioGame) {
            $ratingsMario = $marioGame->ratings;
            foreach($ratingsMario as $ratingMario) {
                $ratingBoard = RatingBoard::where('id', '=',$ratingMario->rating_board_id)->first();
                $res .= " Rating Board : $ratingBoard->name ($ratingBoard->deck) <br>";
            }
        }
        echo $res;
    }






/*
    $formulaireQuestion5 .= " </select> <input type='submit' value='Valider'> </form> ";
    echo $formulaireQuestion5." </body></html> ";
  }*/



  public function personnagesNomJeuMario(){
      $games = Game::where("name", "like", "%mario%")->get();

      foreach ($games as $g){
          $persos = $g->personnages();
          
      }
  }
}
