<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 26/03/2019
 * Time: 14:11
 */

namespace applibd\controleurs;


use applibd\models\Character;
use applibd\models\Game;
use applibd\models\Platform;
use applibd\models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Slim;
use Illuminate\Contracts\Pagination\Paginator;

class TD5_controleur{

    public function partie1($id){
        $app = Slim::getInstance();

        try{
            $jeu = Game::select('id', 'name', 'alias', 'deck', 'description', 'original_release_date')
            ->where("id", "=", $id)
            ->firstOrFail();
        }catch (ModelNotFoundException $e){
            $app->response->setStatus(404);
            $app->response->headers->set("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'game_not_found']);
        }


        //PARTIE 6
        //on récupère ses plateformes
        $plateformes = Game::find($id)->plateformes()->select("id", "name", "alias", "abbreviation")->get();
        //on va lui rajouter ses liens
        foreach ($plateformes as $p){
            $url = $app->urlFor("platform", ["id" => $p->id]);
            $p->links = [
                "self" => ["href" => $url],
            ];
            //pour supprimer la colone "pivot"
            unset($p->pivot);
        }

        //on ajoute les plateformes
        $jeu->plateformes = $plateformes;


        //on rajoute une partie liens
        $comment = $app->urlFor("comments", ["id" => $jeu->id]);
        $persos = $app->urlFor("characters", ["id" => $jeu->id]);
        $jeu->links = [
            "comments" => ["href" => $comment],
            "characters" => ["href" => $persos]
        ];


        //sinon on met un status ok
        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");

        echo json_encode($jeu->toArray());
    }

    public function partie2(){
        $app = Slim::getInstance();

        //on récupère les 200 premiers
        $jeux200 = Game::select('id', 'name', 'alias', 'deck', 'description', 'original_release_date')->take(200)->get();

        //le code de réponse
        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");

        echo json_encode($jeux200->toArray());
    }

    public function partie3(){
        $app = Slim::getInstance();

        //on récupère les 200 premiers
        $jeux200 = Game::select('id', 'name', 'alias', 'deck', 'description', 'original_release_date')->skip(200*$_GET["page"])->take(200)->get();

        //le code de réponse
        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");


        //pour si jamais on est sur la première page
        if($_GET["page"] == 1){
            $pa = 1;
        }else{
            $pa = $_GET["page"]-1;
        }

        //je ne sais pas comment trouver si on est sur la dernière
        if($_GET["page"] == -1){
            $pp = 1;
        }else{
            $pp = $_GET["page"]+1;
        }

        //Partie 4:

        foreach ($jeux200 as $j){
            //récupère l'url
            $url = $app->urlFor("jeu", ["id" => $j->id]);

            $j->links = ["self" =>
                ["href"=> $url]
            ];
        }

        $pagAv = $app->urlFor("jeux") . $pa;
        $pagAp = $app->urlFor("jeux") . $pp;

        $tabfinal["games"] = $jeux200;
        $tabfinal["links"] = [
            "prev" => ["href" => "$pagAv"],
            "next" => ["href" => "$pagAp"]
        ];

        echo json_encode($tabfinal);
    }

    public function partie5($id){
        $app = Slim::getInstance();

        //marche en 1 requête
        //$commentaires = Game::find($id)->commentaires()->select("id", "titre", "contenu", "dateCrea", "idUser")->with("publiePar:id,nom")->get();
        $commentaires = Game::find($id)->commentaires()->select("id", "titre", "contenu", "dateCrea", "idUser")->get();

        foreach ($commentaires as $com){
            $com->nom = User::find($com->idUser)->nom;
            unset($com->idUser);
        }

        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");

        echo json_encode($commentaires);
    }

    public function characters($id){
        $app = Slim::getInstance();

        //récupère les persos du jeu
        $persos = Game::find($id)->personnages()->select("id", "name")->get();

        //on rajoute le lien
        foreach ($persos as $p){
            $lien = $app->urlFor("1character", ["id" => $p->id]);
            $p->links = [
                "self" => ["href" => $lien]
            ];

            //on enlève le pivot
            unset($p->pivot);
        }

        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");

        echo json_encode($persos);
    }

    public function character($id){
        $app = Slim::getInstance();

        //on récupère le perso
        $p = Character::find($id);

        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");

        echo json_encode($p);
    }

    public function platform($id){
        $app = Slim::getInstance();

        $platerforme = Platform::find($id);

        $app->response->setStatus(200);
        $app->response->headers->set("Content-Type", "application/json");

        echo json_encode($platerforme);
    }

}