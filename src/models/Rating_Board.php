<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 13/03/2019
 * Time: 15:39
 */

namespace applibd\models;


use Illuminate\Database\Eloquent\Model;

class Rating_Board extends Model{
    protected $table = "rating_board";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function ratings(){
        return $this->hasMany("applibd\models\Game_rating", "rating_board_id");
    }
}