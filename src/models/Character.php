<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 13/03/2019
 * Time: 15:34
 */

namespace applibd\models;


use Illuminate\Database\Eloquent\Model;

class Character extends Model{
    protected $table = "character";
    protected $primaryKey = "id";
    public $timestamps = false;


    public function enemies(){
        return $this->belongsToMany("applibd\models\Character", "enemies","char1_id", "char2_id");
    }

    public function friends(){
        return $this->belongsToMany("applibd\models\Character", "friends","char1_id", "char2_id");
    }

    public function apparitions(){
        return $this->belongsToMany("applibd\models\Game", "game2character", "character_id", "game_id");
    }
}