<?php
namespace applibd\models;

class User extends \Illuminate\Database\Eloquent\Model{
	protected $table='user';
	protected $primaryKey='id';
	public $timestamps = false;

    public function commentaires(){
        return $this->hasMany("applibd\models\Commentaire", "idUser");
    }
 }

