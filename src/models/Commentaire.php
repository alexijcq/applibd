<?php
namespace applibd\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'commentaire';
	protected $primaryKey = 'id';
	public $timestamp = true;

	public function publiePar(){
		return $this->belongsTo("\applibd\models\User","idUser");
	}

	public function pourUnJeu(){
        return $this->belongsTo("\applibd\models\Game","id");
    }
}
