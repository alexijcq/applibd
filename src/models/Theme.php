<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 13/03/2019
 * Time: 15:43
 */

namespace applibd\models;


use Illuminate\Database\Eloquent\Model;

class Theme extends Model{
    protected $table = "theme";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function games(){
        return $this->belongsToMany("applibd\models\Game", "game2theme", "theme_id", "game_id");
    }
}