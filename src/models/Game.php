<?php
namespace applibd\models;
final class Game extends \Illuminate\Database\Eloquent\Model{
    //Nom de la table.
    protected $table = 'game';
    //Cle primaire de la table.
    protected $primaryKey = 'id' ;
    public $timestamps = false ;

    public function personnagesOntFaitPremiereApparition(){
        return $this->hasMany("applibd\models\Character", "first_appeared_in_game_id");
    }

    public function personnages(){
        return $this->belongsToMany("applibd\models\Character", "game2character", "game_id", "character_id");
    }

    public function genres(){
        return $this->belongsToMany("applibd\models\Genre", "game2genre", "game_id", "genre_id");
    }

    public function plateformes(){
        return $this->belongsToMany("applibd\models\Platform", "game2platform", "game_id", "platform_id");
    }

    public function themes(){
        return $this->belongsToMany("applibd\models\Theme", "game2theme", "game_id", "theme_id");
    }

    public function developers(){
        return $this->belongsToMany("applibd\models\Company", "game_developers", "game_id", "comp_id");
    }

    public function publishers(){
        return $this->belongsToMany('applibd\models\Company', "game_publishers", "game_id", "com_id");
    }

    public function similarGames(){
        return $this->belongsToMany("applibd\models\Game", "similar_games", "game1_id", "game2_id");
    }

    public function ratings(){
        return $this->belongsToMany("applibd\models\Game_rating", "game2rating", "game_id", "rating_id");
    }

    public function commentaires(){
        return $this->hasMany("applibd\models\Commentaire", "idJeu");
    }

}
