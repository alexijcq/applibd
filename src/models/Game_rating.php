<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 13/03/2019
 * Time: 15:36
 */

namespace applibd\models;


use Illuminate\Database\Eloquent\Model;

class Game_rating extends Model{
    protected $table = "game_rating";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function ratingBoard(){
        $this->belongsTo("applibd\models\Rating_board", "rating_board_id");
    }

    public function games(){
        return $this->belongsToMany("applibd\models\Game", "game2rating", "rating_id", "game_id");
    }
}