<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 13/03/2019
 * Time: 15:38
 */

namespace applibd\models;


use Illuminate\Database\Eloquent\Model;

class Genre extends Model{
    protected $table = "genre";
    protected $primaryKey = "id";
    public $timestamps = false;


    public function games(){
        return $this->belongsToMany("applibd\models\Game", "game2genre", "genre_id", "game_id");
    }

}