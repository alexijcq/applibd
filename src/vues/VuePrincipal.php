<?php

namespace applibd\vues;

class VuePrincipal
{
    private $elements;
    private $selecteur;
    private $app;

    public function __construct($tabAffichage, $selecteur)
    {
        $this->elements = $tabAffichage;
        $this->selecteur = $selecteur;
        $this->app = \Slim\Slim::getInstance();
    }

    public function htmlquestion()
    {
            $html = <<<END
<h3>Projet 1</h3>
<ul>
  <li><a href="/projet1/question/1">Question 1</a></li>
  <li><a href="/projet1/question/2">Question 2</a></li>
  <li><a href="/projet1/question/3">Question 3</a></li>
  <li><a href="/projet1/question/4">Question 4</a></li>
  <li><a href="/projet1/question/5">Question 5</a></li>
</ul>
<h3>Projet 2</h3>
<ul>
  <li><a href="/projet2/question/1">Question 1</a></li>
  <li><a href="/projet2/question/2">Question 2</a></li>
  <li><a href="/projet2/question/3">Question 3</a></li>
  <li><a href="/projet2/question/4">Question 4</a></li>
  <li><a href="/projet2/question/5">Question 5</a></li>
  <li><a href="/projet2/question/6">Question 6</a></li>
  <li><a href="/projet2/question/7">Question 7</a></li>
  <li><a href="/projet2/question/8">Question 8</a></li>
</ul>
<h3>Projet 3</h3>
<ul>
  <li><a href="/projet3/question/1">Question 1</a></li>
  <li><a href="/projet3/question/2">Question 2</a></li>
  <li><a href="/projet3/question/3">Question 3</a></li>
  <li><a href="/projet3/question/4">Question 4</a></li>
  <li><a href="/projet3/question/5">Question 5</a></li>
  <li><a href="/projet3/question/5">Question 6</a></li>
  <li><a href="/projet3/question/5">Question 7</a></li>
  <li><a href="/projet3/question/5">Question 8</a></li>
  <li><a href="/projet3/question/5">Question 9</a></li>
</ul>
<h3>Projet 4</h3>
<ul>
  <li><a href="/projet3/question/1">Question 1</a></li>
  <li><a href="/projet3/question/2">Question 2</a></li>
  <li><a href="/projet3/question/3">Question 3</a></li>
</ul>
END;

        return $html;
    }

    public function htmlquestionP1Q1()
    {
        $res = "<h3>Question n°1 : liste des jeux contenant Mario dans leur titre</h3>";
        foreach($this->elements as $game) {
            $res = $res."<p>Nom du jeu : $game->name </p>";
        }
        return $res;
    }
    public function htmlquestionP1Q2()
    {
        $html = <<<END
<p>question</p>
END;
        return $html;
    }
    public function htmlquestionP1Q3()
    {
        $html = <<<END
<p>question</p>
END;
        return $html;
    }
    public function htmlquestionP1Q4()
    {
        $html = <<<END
<p>question</p>
END;
        return $html;
    }
    public function htmlquestionP1Q5()
    {
        $html = <<<END
<p>question</p>
END;
        return $html;
    }

    public function htmlquestionP2Q1()
    {
        $html = <<<END
<p>question 1, TD2</p>
END;

        //on affiche tout
        $valeurs = $this->elements;

        //on construit le truc
        foreach($valeurs as $character) {
            $html .= "Personnage : $character->name || Deck : $character->deck <br>";
        }

        return $html;
    }

    public function htmlquestionP2Q2()
    {
        $html = <<<END
<p>question 2, TD2</p>
END;

        $valeurs = $this->elements;

        foreach($valeurs as $marioGame) {
            foreach($marioGame->personnages as $personnage) {
                $html .= "Personnage du jeu commençant par Mario : $personnage->name <br>";
            }
            $html.= "<br>";
        }
        return $html;
    }

    public function htmlquestionP2Q3()
    {
        $html = <<<END
<p>question 3, TD2</p>
END;

        $elem = $this->elements;


        foreach($elem as $sonyCompany){
            $sonyGames = $sonyCompany->jeuxPublies;
            foreach($sonyGames as $sonyGame) {
                $html .= "Jeu Sony : $sonyGame->name <br>";
            }
        }

        return $html;
    }

    public function htmlquestionP2Q4()
    {
        $html = <<<END
<p>question 4, TD2</p>
END;

        foreach ($this->elements as $rat){
            $html .= " Rating Board : $rat->name ($rat->deck) <br>";
        }
        return $html;
    }

    public function htmlquestionP2Q5(){
        $html = <<<END
<p>question 5, TD2</p>
END;

        foreach ($this->elements as $nom){
            $html .= "Ce jeux a plus de 3 personnages $nom <br>";
        }
        return $html;
    }

    public function htmlquestionP2Q6(){
        $html = <<<END
<p>question 6, TD2</p>
END;

        foreach ($this->elements as $nom){
            $html .= " $nom <br>";
        }
        return $html;
    }

    public function htmlquestionP2Q7(){
        $html = <<<END
<p>question 7, TD2</p>
END;

        foreach ($this->elements as $nom){
            $html .= " $nom <br>";
        }
        return $html;
    }

    public function render()
    {
        switch ($this->selecteur) {
            case "ALL_VIEW" :
                {
                    $content = $this->htmlquestion();
                    break;
                }
            case "P1Q1" :
                {
                    $content = $this->htmlquestionP1Q1();
                    break;
                }
            case "P1Q2" :
                {
                    $content = $this->htmlquestionP1Q2();
                    break;
                }
            case "P1Q3" :
                {
                    $content = $this->htmlquestionP1Q3();
                    break;
                }
            case "P1Q4" :
                {
                    $content = $this->htmlquestionP1Q4();
                    break;
                }
            case "P1Q5" :
                {
                    $content = $this->htmlquestionP1Q5();
                    break;
                }
            case "P2Q1" :
                {
                    $content = $this->htmlquestionP2Q1();
                    break;
                }
            case "P2Q2" :
                {
                    $content = $this->htmlquestionP2Q2();
                    break;
                }
            case "P2Q3" :
                {
                    $content = $this->htmlquestionP2Q3();
                    break;
                }
            case "P2Q4" :
                {
                    $content = $this->htmlquestionP2Q4();
                    break;
                }
            case "P2Q5":
                $content = $this->htmlquestionP2Q5();
                break;

            case "P2Q6":
                $content = $this->htmlquestionP2Q6();
                break;

            case "P2Q7":
                $content = $this->htmlquestionP2Q7();
                break;
        }
        $html = <<<END
<!DOCTYPE html>
<html>
<head><a href="/">accueil</a></head>
<body>
 
<div>
 $content
</div>
</body></html>
END;

        echo $html;
    }
}
